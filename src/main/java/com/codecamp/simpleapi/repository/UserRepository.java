package com.codecamp.simpleapi.repository;

import com.codecamp.simpleapi.domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

}
