package com.codecamp.simpleapi.controller;

import com.codecamp.simpleapi.domain.User;
import com.codecamp.simpleapi.service.UserService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/users")
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("")
    public Iterable<User> list() {
        Integer seconds = 1;

        try {
            Thread.sleep(seconds*1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return userService.list();
    }

    @GetMapping("{id}")
    public User getById(@PathVariable() Long id) {
        return userService.getById(id);
    }

    @PostMapping("")
    public User saveUser(@RequestBody User user) {
        return userService.save(user);
    }
}
